## @file lab6.py
#  Brief doc for lab6.py
#
#  Detailed doc for lab6.py
#  This is the program that graphs my calculations for lab 6.
#
#  @author Fernando Estevez
#
#  @copyright License Info
#
#  @date Febuary 19, 2021
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt

## Radius of lever
rm = 0.06 
## length of rod
lr = 0.05 
## radius of ball
rb = 0.0105 
## Vertical distance from U-Joint to CG of platform
rg = 0.042 
## Horizontal Diostance from U-joint to push rod pivot
lp = 0.110 
## Vertical Distance from U-joint to push rod pivot
rp = 0.0325 
## Vertical Distance from U-joint to platform surface
rc = 0.05 
## Mass of ball
mb = 0.03 
## Mass of platform
mp = 0.400 
## Moment of Inertia of platform 
Ip = 0.00188
## Viscous Friction at u-joint
b = 0.010 
## Gravity
g = 9.8 
## Moment of inertia of ball
Ib = 0

## Solve Equation
# function used to run an analysis based on my linerized equation.
#
# @param time. Time to run the system
# @param stateVec. State Vector [x, theta, dx, dtheta, Tx]
def solveEquation(time, stateVec):
    
    x = stateVec[0]
    theta_y = stateVec[1]
    dx = stateVec[2]
    dtheta = stateVec[3]
    Tx = stateVec[4]
    
    dxdt = dx
    dthetadt = dtheta
    dx2dt = -1*(g*theta_y)
    dtheta2dt = -(rb*x*(g*lp*mb**2*rb**2 + g*lp*rp*mb**2*rb 
                        + Ib*g*lp*mb))/(lp*(mb*mp*rb**2*rg**2 
                        + Ip*mb*rb**2 + Ib*mp*rg**2 + Ib*mb*rp**2 
                        + Ib*Ip)) - (rb*theta_y*(g*lp*mb*mp*rb**2*rg 
                        - g*lp*mb*mp*rb*rg**2 + g*lp*mb*mp*rp*rb*rg
                        - Ip*g*lp*mb*rb + Ib*g*lp*mp*rg + Ib*g*lp*mb*rp))/(
                        lp*(mb*mp*rb**2*rg**2 + Ip*mb*rb**2 + Ib*mp*rg**2 
                        + Ib*mb*rp**2 + Ib*Ip)) - (Tx*rb*(mb*rm*rb**2 
                        + mb*rm*rp*rb + Ib*rm))/(lp*(mb*mp*rb**2*rg**2 
                        + Ip*mb*rb**2 + Ib*mp*rg**2 + Ib*mb*rp**2 + Ib*Ip))
    dTxdt = 0
    
    return[dxdt, dthetadt, dx2dt, dtheta2dt,dTxdt]

## Solve Equation Closed Loop
# function used to run an analysis based on my linerized equation for a 
# closed loop system.
# 
# K=[−0.05 N⋅s−0.02 N⋅m⋅s−0.3 N−0.2 N⋅m]
#
# @param time. Time to run the system
# @param stateVec. State Vector [x, theta, dx, dtheta, Tx]
def solveEquationClosed(time, stateVec):
    
    x = stateVec[0]
    theta_y = stateVec[1]
    dx = stateVec[2]
    dtheta = stateVec[3]
    Tx = 0.05*dx - 0.02*dtheta + 0.3*x - 0.2*theta_y
    
    dxdt = dx
    dthetadt = dtheta
    dx2dt = -1*(g*theta_y)
    dtheta2dt = -(rb*x*(g*lp*mb**2*rb**2 + g*lp*rp*mb**2*rb 
                        + Ib*g*lp*mb))/(lp*(mb*mp*rb**2*rg**2 
                        + Ip*mb*rb**2 + Ib*mp*rg**2 + Ib*mb*rp**2 
                        + Ib*Ip)) - (rb*theta_y*(g*lp*mb*mp*rb**2*rg 
                        - g*lp*mb*mp*rb*rg**2 + g*lp*mb*mp*rp*rb*rg
                        - Ip*g*lp*mb*rb + Ib*g*lp*mp*rg + Ib*g*lp*mb*rp))/(
                        lp*(mb*mp*rb**2*rg**2 + Ip*mb*rb**2 + Ib*mp*rg**2 
                        + Ib*mb*rp**2 + Ib*Ip)) - (Tx*rb*(mb*rm*rb**2 
                        + mb*rm*rp*rb + Ib*rm))/(lp*(mb*mp*rb**2*rg**2 
                        + Ip*mb*rb**2 + Ib*mp*rg**2 + Ib*mb*rp**2 + Ib*Ip))
    dTxdt = 0
    
    return[dxdt, dthetadt, dx2dt, dtheta2dt,dTxdt]

## Garph my equation
#
# Function used to analys and grap the system. Outputs a plot and saves it as
# a .png file
#
# @param time. Time to run the system
# @param stateVec. State Vector [x, theta, dx, dtheta, Tx]
# @param problem. String that idicates the problem section from the doc file
def graphMyEquation(time, stateVec, problem):
    x = stateVec[0]
    theta_y = stateVec[1]
    dx = stateVec[2]
    dtheta = stateVec[3]
    Tx = stateVec[4]
    
    results_a = solve_ivp(solveEquation, [0,time], [x,theta_y,dx,dtheta,Tx], 
                          rtol = 1e-7)
    
    graph_names = ["Position vs. Time", "Angle of platform vs. Time", 
                   "Velocity vs. Time", "Angular Velocity vs. Time"]
    
    y_axis = ["Position(m)", "Angle(rad)", "Velocity(m/s)", 
              "Angular Velocity(rad/s)"]
    
    names = ["position", "angle", "velocity", "angular_velocity"]
    for i in range(len(graph_names)):
        plt.plot(results_a.t, results_a.y[i])
        plt.title(graph_names[i])
        plt.xlabel("time(t)")
        plt.ylabel(y_axis[i])
        plt.savefig(problem + names[i])
        plt.show()
        plt.close()

## Garph my equation for a Closed loop
#
# Function used to analys and grap the system for a closed loop. 
# Outputs a plot and saves it as a .png file
#
# @param time. Time to run the system
# @param stateVec. State Vector [x, theta, dx, dtheta, Tx]
def graphMyEquationClosed(time, stateVec):
    x = stateVec[0]
    theta_y = stateVec[1]
    dx = stateVec[2]
    dtheta = stateVec[3]
    Tx = stateVec[4]
    
    results_a = solve_ivp(solveEquationClosed, [0,time], 
                          [x,theta_y,dx,dtheta,Tx], rtol = 1e-7)
    
    graph_names = ["Position vs. Time", "Angle of platform vs. Time", 
                   "Velocity vs. Time", "Angular Velocity vs. Time"]
    
    y_axis = ["Position(m)", "Angle(rad)", "Velocity(m/s)", 
              "Angular Velocity(rad/s)"]
    
    names = ["position", "angle", "velocity", "angular_velocity"]

    for i in range(len(graph_names)):
        plt.plot(results_a.t, results_a.y[i])
        plt.title(graph_names[i])
        plt.xlabel("time(t)")
        plt.ylabel(y_axis[i])
        plt.savefig("Closed_loop" + names[i])
        plt.show()
        plt.close()     
        
## Simulation for problem 3a
stateVec = (0,0,0,0,0)
graphMyEquation(1, stateVec, "a_")

## Simulation for problem 3b
stateVec = (0.05,0,0,0,0)
graphMyEquation(0.4, stateVec, "b_")

## Simulation for problem 3c
stateVec = (0,0.0872665,0,0,0)
graphMyEquation(0.4, stateVec, "c_")

## Simulation for problem 3d
stateVec = (0,0,0,0,0.001)
graphMyEquation(0.4, stateVec, "d_")

## Simulation for problem 4 closed loop
stateVec = (0.05,0,0,0,0)
graphMyEquationClosed(20, stateVec)



