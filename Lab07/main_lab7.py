## @file main_lab7.py
#  Breif doc for main_lab7.py
#
#  Detailed doc for main_lab7.py
#  This is the main program to test my touch panel class
#
#  @author Fernando Estevez
#
#  @copyright License Info
#
#  @date Febuary 27, 2021
from TouchPanel import TouchPanel
import pyb

## main
#
#  Runs the touch scan 1000 times and calculates the average time(us) it took 
#  to scan the x, y, and z.
#
def main():
    # Define all our pins used for Lab 7
    xp = pyb.Pin.board.PA7
    yp = pyb.Pin.board.PA6
    xm = pyb.Pin.board.PA1
    ym = pyb.Pin.board.PA0
    # Dimentions of the touch panel
    width = 99
    length = 176
    # Center of panel
    center = (88,49)
    
    # Creating a touch screen object 
    touch = TouchPanel(xm, ym, xp, yp, width, length, center)
    
    # Scan 1000 times 
    for i in range(1000):
        print(touch.scan())
        print("\n")
        
    # average time to run this is 903.5034  
    print("Average time(us): " + str(touch.get_Total_Time()/i))

    
if __name__ == "__main__":
    main()