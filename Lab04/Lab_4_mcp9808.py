'''
@file Lab_4_mcp9808.py
@brief A file that runs on the Nucleo L476
@details A file that records temperature values using a MCP9808 sensor
@author Kevin Lee and Fernando Estevez
@date February 9, 2021
'''


import pyb
from pyb import I2C
import utime



class TempSensor():
    '''
    @brief Class that records temperatures
    @details Class that records temperature using a MCP9808 sensor and the built in Nucleo temperature sensor
    '''
    
    def __init__(self, i2c, address):
        '''
        @brief constructor for TempSensor Class
        @details initializes variables for TempSensor Class
        @param i2c I2C Master object
        @param address integer for i2c address 
        '''
        ##I2C object
        self.i2c = i2c
        ##sensor address
        self.address = address
        ##registry for temperature sensor
        self.temp_reg = 5
        ##list of time values in milliseconds
        self.times = []
        ##list of temperatures recorded with the MCP9808
        self.temps = []   
        ##list of ambient temperatures recorded with the MCP9808
        self.atemps = []  
        ##start time in ms
        self.start_time = utime.ticks_ms()
        ##next time in ms
        self.next_time = utime.ticks_ms()
        ##time between datapoints in ms
        self.tick_gap = 60000

    def check(self):
        '''
        @brief checks if the I2C device is ready
        @return Boolean returns true if the I2C device is ready
        '''
        return(self.i2c.is_ready(24))
    
    def board_temp(self):
        '''
        @brief returns the board temperature using the built in temperature sensor
        @return float returns a negative number for some reason
        '''
        self.adcall = pyb.ADCAll(12, 0x70000) # 12 bit resolution, internal channels
        return(self.adcall.read_core_temp())
    
    def celcius(self):
        '''
        @brief returns temperature using MCP9808 in C
        @returns float temperature in C
        @details from https://learn.adafruit.com/micropython-hardware-i2c-devices/i2c-main
        '''
        data = self.i2c.mem_read(4, self.address, self.temp_reg, timeout=5000, addr_size=8)
        value = data[0] << 8 | data[1]
        temp = (value & 0xFFF) / 16.0
        if value & 0x1000:
            temp -= 256.0
        return temp
    
    def fahrenheit(self):
        '''
        @brief returns temperature using MCP9808 in F
        @returns float temperature in F
        '''
        return (self.celcius()*9/5)+32
    
    def export_data(self):
        '''
        @brief exports data to a file called data.csv on the Nucleo
        @details adapted from lab document
        '''
        with open ("data.csv", "w") as a_file:
            for i in range(len(self.temps)):
                # a_file.write ("A line: {:}\r\n".format (line))
                a_file.write(str(self.times[i]) + "," + str(self.temps[i]) + "," + str(self.atemps[i]) + "\r\n")
        print ("The file has by now automatically been closed.")
    
    def add_data(self):
        '''
        @brief adds temperature and time data to lists
        '''
        self.times.append(utime.ticks_diff(utime.ticks_ms(), self.start_time))
        self.temps.append(self.fahrenheit())
        self.atemps.append(self.board_temp())
        print("data added: " + str(len(self.temps)))
    
    def loop(self):
        '''
        @brief collects data until a keyboard interupt occurs
        '''
        while True:
            try:
                if utime.ticks_diff(utime.ticks_ms(),self.next_time)>=0:
                    self.add_data()
                    self.next_time = utime.ticks_add(utime.ticks_ms(), self.tick_gap) 
            except KeyboardInterrupt:
                self.export_data()
                break