## @file mainpage.py
#  Documentation for / use of mainpage.py
#
#  Detailed doc for mainpage.py
#  @mainpage
#  @section sec_port Portfolio Details
#  This is my ME405 Portfolio. See individual modules for detals.
#
#  Included modules are:
#   *Lab0x00 (\ref sec_lab0)
#   *Lab0x01 (\ref sec_lab1)
#
#  @section sec_lab0 Lab0x00 Documentation
#   *Source: https://bitbucket.org/festevez10/me405_labs/src/master/Lab00/
#   *Documentation: \ref Lab0
#
#  @section sec_lab1 Lab0x01 Documentation
#   *Source: https://bitbucket.org/festevez10/me405_labs/src/master/Lab01/
#   *Documentation: \ref Lab1
#
#  @author Fernando Estevez Ramirez
#
#  @date January 12, 2021