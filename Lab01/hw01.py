## @file hw01.py
#  Brief doc for hw01.py
#
#  Detailed doc for hw01.py
#  Implemented functions that would return change as a decimal value and 
#  return a tuple of coins. 
#
#  @author Fernando Estevez
#
#  @copyright License Info
#
#  @date January 14, 2021
#
#  @package hw01
#  Brief doc for the hw01 program functions
#
#  Detailed doc for hw01 program functions
#
#  @author Your Fernando Estevez
#
#  @copyright License Info
#
#  @date January 14, 2021

# Constants that represent the values of 
# Pennys, nickles, dimes,...
PENNY = .01
NICKLE = .05
DIME = .10
QUARTER = .25
ONE_DOLLAR = 1.0
FIVE_DOLLAR = 5.0
TEN_DOLLAR = 10.0
TWENTIE_DOLLAR = 20.0

## Return change as a tuple.
#
#  Takes in a payment as a tuple and a price as an integer
#  Returns the change.
#
#  @param price
#  @param payment 
#  @return change as a tuple.
def getChange(price, payment):
    # Holds the decimal value of the payment 
    payment_val = get_payment_val(payment)
    # Convert the price into a decimal value
    price_val = price * PENNY
    #Calculate the change 
    change = abs(price_val - payment_val)
    return get_change_tuple(round(change, 2))

## Converts the tuple value into a decimal value.
#
#  Takes the payment as a tuple and converts the payment into a decimal value.
#
#  @param payment The tuple amount the user inputs.
#  @return payment as a double.
def get_payment_val(payment):
    payment_val = 0
    payment_val +=payment[0]*PENNY
    payment_val += payment[1]*NICKLE 
    payment_val += payment[2]*DIME
    payment_val += payment[3]*QUARTER 
    payment_val += payment[4]*ONE_DOLLAR 
    payment_val += payment[5]*FIVE_DOLLAR 
    payment_val += payment[6]*TEN_DOLLAR 
    payment_val += payment[7]*TWENTIE_DOLLAR
    return payment_val

## Converts a decimal value to a touble
#
#  Takes the decimal value of change and converts it into 
#  Pennys, Niickles, Dimes, Quarters, Dollars, 5 Dollars, 10 Dollars,
#  and 20 Dollars.
#
#  @param change The decimal value
#  @return Coversion from decimal to tuple.
def get_change_tuple(change):
    # Array of diffent bills and coins 
    money = [TWENTIE_DOLLAR, TEN_DOLLAR, FIVE_DOLLAR, ONE_DOLLAR, QUARTER, DIME, NICKLE, PENNY]
    # this will hold our data for the tuple output
    change_t = [0, 0, 0, 0, 0, 0, 0, 0]
    # Keeps track of change
    val = abs(change)
    temp = 0
    # iterate through the entire money types
    for i in range(8):
        #see if the value is divisable by a certain money type
        # we dont care about decimals so we int cast it 
        temp = int( val / money[i])
        # if it is record the number of times it was divisable by
        if (temp > 0):
            # update the change array to the amount the val was divisable by
            change_t[i] = temp
            # update the new value
            val = val - (temp * money[i])
    #return a tuple        
    return (change_t[7], change_t[6], change_t[5], change_t[4], change_t[3], change_t[2], change_t[1], change_t[0])

if __name__ == "__main__":
    payment = (3, 0 ,0, 2, 1, 0, 0, 1)
    price = 3250
    print(getChange(price, payment))