## @file VendingMachine.py
#  Brief doc for VendingMachine.py
#
#  Detailed doc for VendingMachine.py
#  Implemented a Vending Machine using an FSM 
#
#  @author Fernando Estevez
#
#  @copyright License Info
#
#  @date January 14, 2021
#
#  @package VendingMachine
#  Brief doc for the Vending Machine program
#
#  Detailed doc for Vending Machine program
#
#  @author Your Fernando Estevez
#
#  @copyright License Info
#
#  @date January 14, 2021
import keyboard
import constant
import hw01
import sys

state = 0
sufficeint_funds = False
eject = True
price = 0

## Gets the total change.
#
#  Takes in a payment and subtracts it by the price of the drink.
#
#  @param payment
#  @return the change as a decimal
def getChange(payment):
    return price - payment 

## Prints a welcome message.
#
#  At start up, the system displayes a message to the user.
#
def printWelcome():
    print("Welcome, please insert coins")
    print("Enter 0 for penney, 1 for nickle, 2 for dime and so on")
    pass

## Listen's to a keypress and takes action.
#
#  Waits for the user to input a key press and takes an action.
#  Types of input keys:
#  e = eject -> returns your change     0 = Pennys      4 = 1 Dollars
#  c = Cuke                             1 = Nickles     5 = 5 Dollars
#  p = Popsi                            2 = Dimes       6 = 10 Dollars
#  s = Spryte                           3 = Quarters    7 = 20 Dollars
#  d = Dr.Pupper
#  @param payment 
def keyPressed(payment, funds):
    global sufficeint_funds, price, eject 
    eject = False
    while True:
        try:
            keyboard.read_key()
            if keyboard.is_pressed('e'):
                print('\neject')
                eject = True
                break
            
            if keyboard.is_pressed('c'):
                if(funds >= constant.CUKE):
                    sufficeint_funds = True
                    price = constant.CUKE
                    break
                else:
                    sufficeint_funds = False
                    print("\nPrice of Cuke is 1.50")
                    print("Insuficent funds\n")
                break
            
            if keyboard.is_pressed('p'):
                if(funds >= constant.POPSI):
                    sufficeint_funds = True
                    price = constant.POPSI
                    break
                else:
                    sufficeint_funds = False
                    print("\nPrice of Popsi is $0.75")
                    print("Insuficent funds\n")  
                break
            
            if keyboard.is_pressed('s'):
                if(funds >= constant.SPRYTE):
                    sufficeint_funds = True
                    price = constant.SPRYTE
                    break
                else:
                    sufficeint_funds = False
                    print("\nPrice of Spryte is $1.25")
                    print("Insuficent funds\n")
                break
            
            if keyboard.is_pressed('d'):
                if(funds >= constant.DR_PUPPER):
                    sufficeint_funds = True
                    price = constant.DR_PUPPER
                    break
                else:
                    sufficeint_funds = False
                    print("\nPrice of Dr.Pupper is $0.25")
                    print("Insuficent funds\n")
                
                break
            if keyboard.is_pressed('0'):
                payment[constant.PENNY_IDX] += 1 
                print("\nPennys: {}\n".format(payment[constant.PENNY_IDX]))
                break
            if keyboard.is_pressed('1'):
                payment[constant.NICKLE_IDX] += 1 
                print("\nNickles: {}\n".format(payment[constant.NICKLE_IDX]))                
                break
            if keyboard.is_pressed('2'):
                payment[constant.DIME_IDX] += 1 
                print("\nDimes: {}\n".format(payment[constant.DIME_IDX]))
                break
            if keyboard.is_pressed('3'):
                payment[constant.QUARTER_IDX] += 1 
                print("\nQuarter: {}\n".format(payment[constant.QUARTER_IDX]))
                break
            if keyboard.is_pressed('4'):
                payment[constant.DOLLAR_IDX] += 1 
                print("\nDollar: {}\n".format(payment[constant.DOLLAR_IDX]))
                break
            if keyboard.is_pressed('5'):
                payment[constant.FIVE_IDX] += 1 
                print("\nFive Dollars: {}\n".format(payment[constant.FIVE_IDX]))
                break
            if keyboard.is_pressed('6'):
                payment[constant.TEN_IDX] += 1 
                print("\nTen Dollars: {}\n".format(payment[constant.TEN_IDX]))
                break
            if keyboard.is_pressed('7'):
                payment[constant.TWENTY_IDX] += 1 
                print("\nTwenty Dollars: {}\n".format(payment[constant.TWENTY_IDX]))
                break
        #CTRL-C triggers an interupt when pressed    
        except KeyboardInterrupt:
            keyboard.unhook_all()
            sys.exit()
            break
        
## Check for user money input
#
#  Boolean function that checks to see if a user has gaven the system money 
#
#  @param payment
#  @return True if user has entered a coin value. False if no input is given.
def hasInserted(payment):
    for i in range(8):
        if payment[i] != 0:
            return True
    return False

## Gets the selected drink
#
#  Returns the selected drink as a string
#
#  @param Price The price of the drink
#  @return The type of drink.
def getDrink(price):
    if(price == constant.DR_PUPPER):
        return "Dr. Pupper"
    elif price == constant.POPSI:
        return "Popsi"
    elif price == constant.SPRYTE:
        return "Spryte"
    elif price == constant.CUKE:
        return "cuke"
    return " "   

## FSM for the Vending Machine
#
if __name__ == "__main__":
    # Array that holds user payment
    funds = 0
    
    while True:
        # This is the start fo the program.
        if state == 0:
            payment = [0, 0, 0, 0, 0, 0, 0, 0]
            # Print welcome message 
            printWelcome()
            #Check for keyboard presses
            keyPressed(payment, funds)
            #Checks to see if eject was pressed
            if eject == True:
                state = 3
            # Check to see if money has been inserted
            if hasInserted(payment) == True:
                state = 1
        # State 1 = Select your soda
        elif state == 1:
            print("Select your soad.")
            print("Press e, c, p, s, or d for soda selection")
            #converts touple payment into a money value
            funds = hw01.get_payment_val(payment)
            print("Your credits: ${:.2f}".format(funds))
            # Checks for keybored presses
            keyPressed(payment, funds)
            # Checks to see if user ejected
            if eject == True:
                state = 3
            #Checks if user has sufficent funds
            if sufficeint_funds == True:
                state = 2
        # State 2 = Get your drink and change 
        elif state == 2:
            print("\nThank you for your purchase!")
            print("Here is your drink: {}".format(getDrink(price)))
            #Calculates the change
            change = getChange(funds)
            print("Your Change: ${:.2f}".format(abs(change)))
            #Converts the change into a tuple
            changeTup = hw01.get_change_tuple(round(change, 2))
            # Ouputs change
            print(('\nChange:\n'
                   ' %d pennies\n'
                   ' %d nickels\n'
                   ' %d dimes\n'
                   ' %d Quarters\n'
                   ' %d One Dollars\n'
                   ' %d Five Dollars\n'
                   ' %d Ten Dollars\n'
                   ' %d Twenty Dollars\n') % changeTup)
            #Resets variables
            price = 0
            funds = 0
            sufficeint_funds = False
            eject = False;
            state = 0
        # State 3 = eject and give back your money
        elif state == 3:
            #reset eject flag
            eject = False;
            print("Your credits: ${:.2f}".format(funds))
             #Converts the change into a tuple
            changeTup = hw01.get_change_tuple(round(funds, 2))
            # Ouputs change
            print(('\nChange:\n'
                   ' %d pennies\n'
                   ' %d nickels\n'
                   ' %d dimes\n'
                   ' %d Quarters\n'
                   ' %d One Dollars\n'
                   ' %d Five Dollars\n'
                   ' %d Ten Dollars\n'
                   ' %d Twenty Dollars\n') % changeTup)
            
            #reset variables
            price = 0
            funds = 0
            sufficeint_funds = False
            state = 0
      


