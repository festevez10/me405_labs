# -*- coding: utf-8 -*-
## @file constant.py
#  Brief doc for constant.py
#
#  Detailed doc for constant.py 
#  These Constants were used for the vending machine program. They represent 
#  array indexes, Price constants, and payment coin values. 
#
#  @author Fernando Estevez
#
#  @copyright License Info
#
#  @date January 14, 2021
#
#  @package constant
#  Constants used for vending machine Program 
#
#  Detailed doc for constant file
#
#  @author Your Fernando Estevez
#
#  @copyright License Info
#
#  @date January 14, 2021

# Constants for indexing array of payment
PENNY_IDX = 0
NICKLE_IDX = 1
DIME_IDX = 2
QUARTER_IDX = 3
DOLLAR_IDX = 4
FIVE_IDX = 5
TEN_IDX = 6
TWENTY_IDX = 7

# Soda prices constants
CUKE = 1.50
POPSI = 0.75
SPRYTE = 1.25
DR_PUPPER = 0.25

# Constants that represent the values of Pennys, nickles, dimes,...
PENNY = .01
NICKLE = .05
DIME = .10
QUARTER = .25
ONE_DOLLAR = 1.0
FIVE_DOLLAR = 5.0
TEN_DOLLAR = 10.0
TWENTIE_DOLLAR = 20.0